from pymote.algorithm import NodeAlgorithm
from pymote.message import Message
from pymote.sensor import DistSensor
import scipy.stats
import math
import networkx as nx


class Localization(NodeAlgorithm):
	required_params = ('informationKey',)
	default_params = {'neighborsKey': 'Neighbors'}
	
	
	
	def initializer(self):
		origin_node = []
		dist_sensor = DistSensor({'pf': scipy.stats.norm, 'scale': 0 })
		for node in self.network.nodes():
			if self.informationKey in node.memory:
				origin_node = node
				#self.network.outbox.insert(0, Message(header='aaav', destination=origin_node))#NodeAlgorithm.INI
				node.status = 'ORIGIN'
			dist = dist_sensor.read(node)['Dist']
			node.memory['dist']= []
			node.memory['dist'] = dist
			node.send(Message(destination=origin_node, header=node, data=dist))
			
	def origin(self, node, message):
	
		for msg in node.inbox:
			node.memory[msg.header] = []
			node.memory[msg.header] =  msg.data
		
		if not node.inbox:
			X = self.network.nodes()[self.network.number_of_nodes()-1]
			node.memory[X] = X.memory['dist']
			quadsi = []
			dmin = 5
			Measi = node.memory[node].copy()
			for j,dij in Measi.iteritems():
				Measj = node.memory[j].copy()
				Measj_copy = Measj.copy()
				for k,djk in Measj_copy.iteritems():
					Meask = node.memory[k].copy()
					del Meask[j]
					for l,dkl in Meask.iteritems():
						Measl = node.memory[l].copy()
						for m,dlm in Measl.iteritems():
							if m != j:
								continue
							if k not in Measi.keys():
								continue
							dik = Measi[k]
							if l not in Measi.keys():
								continue
							dil = Measi[l]
							dlj = Measl[j]
							if self.isRobust(djk,dkl,dlj,dmin) and self.isRobust(dij,dik,djk,dmin) \
								and self.isRobust(dij,dil,dlj,dmin) and self.isRobust(dik,dil,dkl,dmin):
								tmp1 = (node,j,k,l)
								tmp2 = sorted(tmp1)
								tmp1 = tuple(tmp2)
								if tmp1 not in quadsi:
									quadsi.append(tmp1)
					del Measj[k]

		
			if quadsi:
				overlap = nx.Graph()
				overlap.add_nodes_from(quadsi)
				for i in range(1,len(quadsi)):
					if self.isNeighbour(quadsi[i-1],quadsi[i]):
							overlap.add_edge(quadsi[i-1],quadsi[i])
							
							
				nx.draw(overlap)
				
				LocsBest = []
				for subG in nx.connected_component_subgraphs(overlap):
					Locs = []
					quad = subG.nodes()[0]
					
					distA = quad[0].memory['dist']
					dab = distA[quad[1]]
					dac = distA[quad[2]]
					distB = node.memory[quad[1]]
					dbc = distB[quad[2]]
					
					p0 = (0,0)
					p1 = (dab,0)
					alpha = (dab**2 + dac**2 - dbc**2)/(2*dab*dac)
					p2 = (dac*alpha, dac*math.sqrt(1-alpha))
					
					Locs =[(quad[0],p0), (quad[1], p1), (quad[2],p2)]
					
					visit = []
					bfs_tree = nx.bfs_successors(subG,quad)
					if bfs_tree:
						visit = self.BFS(bfs_tree)
					else:
						visit.append(quad)
					
					for q in visit:
						tmp = []
						for i in Locs:
							tmp.append(i[0])
						if set(q)-set(tmp):
							j=set(q)-set(tmp)
							j=j.pop()
							q=list(q)
							q.remove(j)
							p=[]
							q.sort()
							Locs.sort()
							for x in q:
								for a,b in Locs:
									if x==a:
										p.append((a,b))
							
							pa = p[0][1]
							pb = p[1][1]
							pc = p[2][1]
							
							daj = node.memory[p[0][0]][j]
							dbj = node.memory[p[1][0]][j]
							dcj = node.memory[p[2][0]][j]
							
							p_ = self.trilaterate(pa,daj,pb,dbj,pc,dcj)
							

							Locs.append((j,p_))
					if len(Locs)>len(LocsBest):
						LocsBest=Locs
						
				print LocsBest	
	def trilaterate(self, P1, ra, P2, rb, P3, rc):
		xa = P1[0]
		xb = P2[0]
		xc = P3[0]
		ya = P1[1]
		yb = P2[1]
		yc = P3[1]
		
		# x = (((2*j3-2*j2)*((d1*d1-d2*d2)+(i2*i2-i1*i1)+(j2*j2-j1*j1)) \
			# - (2*j2-2*j1)*((d2*d2-d3*d3)+(i3*i3-i2*i2)+(j3*j3-j2*j2)))\
                # /((2*i2-2*i3)*(2*j2-2*j1)-(2*i1-2*i2)*(2*j3-2*j2)))
				
		# y = ((d1**2-d2**2)+(i2**2-i1**2)+(j2**2-j1**2)+x*(2*i1-2*i2))/(2*j2-2*j1)
		
		S = (pow(xc,2) - pow(xb,2) + pow(yc,2) - pow(yb,2) + pow(rb,2) - pow(rc,2) ) / 2
		T = (pow(xa,2) - pow(xb,2) + pow(ya,2) - pow(yb,2) + pow(rb,2) - pow(ra,2) ) / 2
		
		
		y = ((T*(xb-xc)) - (S*(xb-xa))) / (((ya-yb)*(xb-xc)) - ((yc-yb)*(xb-xc)))
		x = ((y*(ya-yb)) - T) / (xb-xa)
		
		return (x,y)
		
		
	def BFS (self, t):
		visit = []
		
		for a,b in t.items():
			
			if a not in visit:
				visit.append(a)
			for x in b:
				if x not in visit:
					visit.append(x)
					
		return visit
			
	def isNeighbour (self, a, b):
		if len (set(a)-set(b)) == 1:
			return True
		else:
			return False
		
	def angle (self, a, b, c):
		return math.degrees(math.acos((c**2 - b**2 - a**2)/(-2.0 * a * b)))
							
	def isRobust(self, d1,d2,d3,dmin):
		b = min (d1,d2,d3)
		
		angA = self.angle(d1,d2,d3)
		angB = self.angle(d2,d3,d1)
		angC = self.angle(d3,d1,d2)
		
		theta = min (angA,angB,angC)
		
		return b*math.pow(math.sin(theta),2) > dmin			
	
	
	
	STATUS = {
              'ORIGIN': origin
			 
			  }
			
			
