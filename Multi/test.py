from pymote.networkgenerator import NetworkGenerator
import networkx as nx

net_gen = NetworkGenerator(100) 
net = net_gen.generate_random_network()

from pymote.algorithms.bmo.Multi.localization import Localization
net.algorithms = ( (Localization, {'informationKey':'I'}), )

#some_node = net.nodes()[0]               
for some_node in net.nodes():
	some_node.memory['I'] = 'Hello distributed world'

sim = Simulation(net)
sim.run()